import { defineConfig, loadEnv } from "vite";
import { getServiceEnvConfig } from "./.env-config";
import {
	createViteProxy,
	getRootPath,
	getSrcPath,
	setupVitePlugins,
	viteDefine,
} from "./build";

// https://vitejs.dev/config/
export default defineConfig((configEnv) => {
	// 根据当前工作目录中的 `mode` 加载 .env 文件
	const viteEnv = loadEnv(configEnv.mode, process.cwd()) as ImportMetaEnv;

	const rootPath = getRootPath();
	const srcPath = getSrcPath();

	const isOpenProxy = viteEnv.VITE_HTTP_PROXY === "Y";
	const envConfig = getServiceEnvConfig(viteEnv);

	return {
		base: viteEnv.VITE_BASE_URL,
		resolve: {
			alias: {
				// 设置路径
				"~": rootPath,
				// 设置别名
				"@": srcPath,
			},
		},
		define: viteDefine,
		plugins: setupVitePlugins(viteEnv),
		css: {
			preprocessorOptions: {
				scss: {
					// additionalData: `@use "./src/styles/scss/global.scss" as *;`,
				},
			},
		},
		server: {
			host: "0.0.0.0",
			port: 3200,
			open: true,
			proxy: createViteProxy(isOpenProxy, envConfig),
		},
		build: {
			reportCompressedSize: false,
			sourcemap: false,
			commonjsOptions: {
				ignoreTryCatch: false,
			},
		},
	};
});
