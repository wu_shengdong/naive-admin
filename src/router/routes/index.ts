/** 根路由: / */
export const ROOT_ROUTE: AuthRoute.Route = {
	name: "root",
	path: "/",
	redirect: import.meta.env.VITE_ROUTE_HOME_PATH,
	meta: {
		title: "Root",
	},
};

/** 固定的路由 */
export const constantRoutes: AuthRoute.Route[] = [
	ROOT_ROUTE,
	// 匹配无效路径的路由
	{
		name: "not-found",
		path: "/:pathMatch(.*)*",
		component: "blank",
		meta: {
			title: "未找到",
			singleLayout: "blank",
		},
	},
];
