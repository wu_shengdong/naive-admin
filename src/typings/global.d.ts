interface Window {
	$loadingBar?: import("naive-ui").LoadingBarProviderInst;
	$dialog?: import("naive-ui").DialogProviderInst;
	$message?: import("naive-ui").MessageProviderInst;
	$notification?: import("naive-ui").NotificationProviderInst;
}

interface ViewTransition {
	ready: Promise<void>;
}

interface Document {
	startViewTransition?: (
		callback: () => Promise<void> | void
	) => ViewTransition;
}

/** 构建时间 */
declare const PROJECT_BUILD_TIME: string;

interface ScrollbarInst {
	ScrollbarInst?: import("naive-ui").ScrollbarInst;
}
