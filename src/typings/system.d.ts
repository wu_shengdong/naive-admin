/** 请求的相关类型 */
declare namespace Service {
	/**
	 * 请求的错误类型：
	 * - axios: axios错误：网络错误, 请求超时, 默认的兜底错误
	 * - http: 请求成功，响应的http状态码非200的错误
	 * - backend: 请求成功，响应的http状态码为200，由后端定义的业务错误
	 */
	type RequestErrorType = "axios" | "http" | "backend";

	/** 请求错误 */
	interface RequestError {
		/** 请求服务的错误类型 */
		type: RequestErrorType;
		/** 错误码 */
		code: string | number;
		/** 错误信息 */
		msg: string;
	}

	/** 后端接口返回的数据结构配置 */
	interface BackendResultConfig {
		/** 表示后端请求状态码的属性字段 */
		codeKey: string;
		/** 表示后端请求数据的属性字段 */
		dataKey: string;
		/** 表示后端消息的属性字段 */
		msgKey: string;
		/** 后端业务上定义的成功请求的状态 */
		successCode: number | string;
	}

	/** 自定义的请求成功结果 */
	interface SuccessResult<T = any> {
		/** 请求错误 */
		error: null;
		/** 请求数据 */
		data: T;
	}

	/** 自定义的请求失败结果 */
	interface FailedResult {
		/** 请求错误 */
		error: RequestError;
		/** 请求数据 */
		data: null;
	}

	/** 自定义的请求结果 */
	type RequestResult<T = any> = SuccessResult<T> | FailedResult;

	/** 多个请求数据结果 */
	type MultiRequestResult<T extends any[]> = T extends [
		infer First,
		...infer Rest
	]
		? [First] extends [any]
			? Rest extends any[]
				? [Service.RequestResult<First>, ...MultiRequestResult<Rest>]
				: [Service.RequestResult<First>]
			: Rest extends any[]
			? MultiRequestResult<Rest>
			: []
		: [];

	/** 请求结果的适配器函数 */
	type ServiceAdapter<T = any, A extends any[] = any> = (...args: A) => T;

	/** mock示例接口类型：后端接口返回的数据的类型 */
	interface MockServiceResult<T = any> {
		/** 状态码 */
		code: string | number;
		/** 接口数据 */
		data: T;
		/** 接口消息 */
		message: string;
	}

	/** mock的响应option */
	interface MockOption {
		url: Record<string, any>;
		body: Record<string, any>;
		query: Record<string, any>;
		headers: Record<string, any>;
	}
}

/**主题相关类型 */
declare namespace Theme {
	/**主题配置 */
	interface Setting {
		/**暗黑模式 */
		darkMode: boolean;
		/**是否自动跟随系统主题 */
		followSystemTheme: boolean;
		/** 自定义暗黑动画过渡 */
		isCustomizeDarkModeTransition: boolean;
		/** 主题颜色 */
		themeColor: string;
		/** 主题颜色列表 */
		themeColorList: string[];
		/** 其他颜色 */
		otherColor: OtherColor;
		/** 是否自定义info的颜色(默认取比主题色深一级的颜色) */
		isCustomizeInfoColor: boolean;
		/**导航栏模式 */
		navMode: UnionKey.NavMode;
		/**导航栏风格 */
		navTheme: UnionKey.NavTheme;
		/**顶部设置 */
		header: Header;
		/**标签页样式 */
		tab: Tab;
		/**侧边栏样式 */
		sider: Sider;
		scrollbar: ScrollbarInst | null;
	}

	interface Header {
		/**固定顶部 */
		fixed: boolean;
		/**显示重载按钮 */
		isReload: boolean;
		/**面包屑 */
		crumb: Crumb;
	}

	/** 其他主题颜色 */
	interface OtherColor {
		/** 信息 */
		info: string;
		/** 成功 */
		success: string;
		/** 警告 */
		warning: string;
		/** 错误 */
		error: string;
	}

	/**侧边啦样式 */
	interface Sider {
		/**侧边栏折叠状态 */
		collapsed: boolean;
		/**侧边栏宽度 */
		width: number;
		/**侧边栏折叠时的宽度 */
		collapsedWidth: number;
	}

	/**多标签页样式 */
	interface Tab {
		/**是否固定 */
		fixed: boolean;
		/**标签页可见 */
		show: boolean;
	}

	/**面包屑样式 */
	interface Crumb {
		/**面包屑可见 */
		show: boolean;
		/**显示图标 */
		showIcon: boolean;
	}
}

declare namespace App {
	/** 菜单项配置 */
	type GlobalMenuOption = import("naive-ui").MenuOption & {
		key: string;
		label: string;
		routeName: string;
		routePath: string;
		icon?: () => import("vue").VNodeChild;
		children?: GlobalMenuOption[];
		i18nTitle?: I18nType.I18nKey;
	};
}

declare namespace I18nType {
	type LangType = "en" | "zh-CN" | "km-KH";

	type Schema = {
		system: {
			title: string;
		};
		common: {
			add: string;
			addSuccess: string;
			edit: string;
			editSuccess: string;
			delete: string;
			deleteSuccess: string;
			batchDelete: string;
			confirm: string;
			cancel: string;
			pleaseCheckValue: string;
			action: string;
		};
		routes: {
			dashboard: {
				_value: string;
				analysis: string;
				workbench: string;
			};
			document: {
				_value: string;
				vue: string;
				vite: string;
				naive: string;
				project: string;
				"project-link": string;
			};
			component: {
				_value: string;
				button: string;
				card: string;
				table: string;
			};
			plugin: {
				_value: string;
				charts: {
					_value: string;
					antv: string;
					echarts: string;
				};
				copy: string;
				editor: {
					_value: string;
					markdown: string;
					quill: string;
				};
				icon: string;
				map: string;
				print: string;
				swiper: string;
				video: string;
			};
			"auth-demo": {
				_value: string;
				permission: string;
				super: string;
			};
			function: {
				_value: string;
				tab: string;
			};
			exception: {
				_value: string;
				"403": string;
				"404": string;
				"500": string;
			};
			"multi-menu": {
				_value: string;
				first: {
					_value: string;
					second: string;
					"second-new": {
						_value: string;
						third: string;
					};
				};
			};
			management: {
				_value: string;
				auth: string;
				role: string;
				route: string;
				user: string;
			};
			about: string;
		};
		layout: {
			settingDrawer: {
				title: string;
				themeModeTitle: string;
				darkMode: string;
				layoutModelTitle: string;
				systemThemeTitle: string;
				pageFunctionsTitle: string;
				pageViewTitle: string;
				followSystemTheme: string;
				isCustomizeDarkModeTransition: string;
				scrollMode: string;
				scrollModeList: {
					wrapper: string;
					content: string;
				};
				fixedHeaderAndTab: string;
				header: {
					inverted: string;
					height: string;
					crumb: {
						visible: string;
						icon: string;
					};
				};
				tab: {
					visible: string;
					height: string;
					modeList: {
						mode: string;
						chrome: string;
						button: string;
					};
					isCache: string;
				};
				sider: {
					inverted: string;
					width: string;
					mixWidth: string;
				};
				menu: {
					horizontalPosition: string;
					horizontalPositionList: {
						flexStart: string;
						center: string;
						flexEnd: string;
					};
				};
				footer: {
					inverted: string;
					visible: string;
					fixed: string;
					right: string;
				};
				page: {
					animate: string;
					animateMode: string;
					animateModeList: {
						zoomFade: string;
						zoomOut: string;
						fadeSlide: string;
						fade: string;
						fadeBottom: string;
						fadeScale: string;
					};
				};
				systemTheme: {
					moreColors: string;
				};
				themeConfiguration: {
					title: string;
					copy: string;
					reset: string;
					resetSuccess: string;
					operateSuccess: string;
					copySuccess: string;
					confirmCopy: string;
				};
			};
		};
		page: {
			login: {
				common: {
					userNamePlaceholder: string;
					phonePlaceholder: string;
					codePlaceholder: string;
					passwordPlaceholder: string;
					confirmPasswordPlaceholder: string;
					codeLogin: string;
					confirm: string;
					back: string;
					validateSuccess: string;
					loginSuccess: string;
					welcomeBack: string;
				};
				pwdLogin: {
					title: string;
					rememberMe: string;
					forgetPassword: string;
					register: string;
					otherAccountLogin: string;
					otherLoginMode: string;
					superAdmin: string;
					admin: string;
					user: string;
				};
				codeLogin: {
					title: string;
					getCode: string;
					imageCodePlaceholder: string;
				};
				register: {
					title: string;
					agreement: string;
					protocol: string;
					policy: string;
				};
				resetPwd: {
					title: string;
				};
				bindWeChat: {
					title: string;
				};
			};
		};
	};

	type GetI18nKey<
		T extends Record<string, unknown>,
		K extends keyof T = keyof T
	> = K extends string
		? T[K] extends Record<string, unknown>
			? `${K}.${GetI18nKey<T[K]>}`
			: K
		: never;

	type I18nKey = GetI18nKey<Schema>;
}
