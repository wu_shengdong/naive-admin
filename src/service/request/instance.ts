import type { AxiosInstance, AxiosRequestConfig } from "axios";
import axios from "axios";

type RefreshRequestQueue = (config: AxiosRequestConfig) => void;

/**
 * 封装axios请求类
 * @author Soybean<honghuangdc@gmail.com>
 */
export default class CustomAxiosInstance {
	instance: AxiosInstance;

	backendConfig: Service.BackendResultConfig;

	isRefreshing: boolean;

	retryQueues: RefreshRequestQueue[];

	/**
	 *
	 * @param axiosConfig - axios配置
	 * @param backendConfig - 后端返回的数据配置
	 */
	constructor(
		axiosConfig: AxiosRequestConfig,
		backendConfig: Service.BackendResultConfig = {
			codeKey: "code",
			dataKey: "data",
			msgKey: "message",
			successCode: 200,
		}
	) {
		this.backendConfig = backendConfig;
		this.instance = axios.create(axiosConfig);
		this.setInterceptor();
		this.isRefreshing = false;
		this.retryQueues = [];
	}

	/** 设置请求拦截器 */
	setInterceptor() {
		this.instance.interceptors.request.use();
		this.instance.interceptors.response.use();
	}
}
