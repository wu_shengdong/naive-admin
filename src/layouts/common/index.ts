import GlobalContent from "./global-content/index.vue";
import GlobalHeader from "./global-header/index.vue";
import GlobalLogo from "./global-logo/index.vue";
import GlobalMenu from "./global-menu/index.vue";

export { GlobalContent, GlobalHeader, GlobalLogo, GlobalMenu };
