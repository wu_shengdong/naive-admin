import VerticalLayout from "./VerticalLayout/index.vue";
import HorizontalLayout from "./HorizontalLayout/index.vue";

export {
	VerticalLayout,
	HorizontalLayout,
};
