const themeColorList = [
	"#1890ff",
	"#409EFF",
	"#007AFF",
	"#5ac8fa",
	"#5856D6",
	"#536dfe",
	"#646cff",
	"#9c27b0",
	"#AF52DE",
	"#0096c7",
	"#00C1D4",
	"#34C759",
	"#43a047",
	"#7cb342",
	"#c0ca33",
	"#78DEC7",
	"#e53935",
	"#d81b60",
	"#f4511e",
	"#fb8c00",
	"#ffb300",
	"#fdd835",
	"#6d4c41",
	"#546e7a",
];

const themeSetting = {
	/**暗黑模式 */
	darkMode: false,
	/**是否自动跟随系统主题 */
	followSystemTheme: false,
	themeColor: themeColorList[6],
	themeColorList,
	otherColor: {
		info: "#2080f0",
		success: "#52c41a",
		warning: "#faad14",
		error: "#f5222d",
	},
	/** 自定义暗黑动画过渡 */
	isCustomizeDarkModeTransition: false,
	isCustomizeInfoColor: false,
	/**导航栏模式 */
	navMode: "vertical",
	/**导航栏风格 */
	navTheme: "header-dark",
	//顶部
	header: {
		//固定顶部
		fixed: true,
		//显示重载按钮
		isReload: true,
		crumb: {
			show: true,
			showIcon: true,
		},
	},
	//多标签
	tab: {
		//是否显示
		show: true,
		//固定多标签
		fixed: true,
	},
	//菜单
	sider: {
		//最小宽度
		collapsedWidth: 64,
		//菜单宽度
		width: 200,
		// 折叠菜单
		collapsed: false,
	},
	scrollbar: null,
};
export default themeSetting as Theme.Setting;
