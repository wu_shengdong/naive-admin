import { useThemeStore } from "@/store";
import { computed, ref, watch } from "vue";
import { useRouteProps } from "./route";

export function useLayoutConfig() {
	const { setScrollbarInstance } = useThemeStore();
	const routeProps = useRouteProps();

	/** 全局侧边栏的样式 */
	const globalSiderClassAndStyle = {
		class: "transition-all duration-300 ease-in-out",
		style: "z-index:12;box-shadow: 2px 0 8px 0 rgb(29 35 41 / 5%);",
	};

	/** 纵向flex布局样式 */
	const flexColumnStyle = "display:flex;flex-direction:column;height:100%;";

	/** scrollbar的content的样式 */
	const scrollbarContentStyle = computed(() => {
		const { fullPage } = routeProps.value;
		const height = fullPage ? "100%" : "auto";
		return `display:flex;flex-direction:column;height:${height};min-height:100%;`;
	});

	/** 滚动条实例 */
	const scrollbar = ref<ScrollbarInst | null>(null);

	watch(scrollbar, (newValue) => {
		if (newValue) {
			setScrollbarInstance(newValue);
		}
	});

	return {
		globalSiderClassAndStyle,
		flexColumnStyle,
		scrollbarContentStyle,
		scrollbar,
	};
}
