import { getColorPalettes, getRgbOfColor, sessionStg } from "@/utils";
import { kebabCase } from "lodash-es";
import type { GlobalThemeOverrides } from "naive-ui";
import { useOsTheme } from "naive-ui";
import { effectScope, onScopeDispose, watch } from "vue";
import { useThemeStore } from "../modules";

/** 订阅theme store */
export default function subscribeThemeStore() {
	const theme = useThemeStore();
	const osTheme = useOsTheme();
	const scope = effectScope();

	scope.run(() => {
		// 监听主题颜色
		watch(
			() => theme.themeColor,
			(newValue) => {
				sessionStg.set("themeColor", newValue);
			},
			{ immediate: true }
		);

		// 监听naiveUI themeOverrides
		watch(
			() => theme.naiveThemeOverrides,
			(newValue) => {
				if (newValue.common) {
					addThemeCssVarsToHtml(newValue.common);
				}
			},
			{ immediate: true }
		);

		// 监听操作系统主题模式
		watch(
			osTheme,
			(newValue) => {
				const isDark = newValue === "dark";
				theme.setAutoFollowSystemMode(isDark);
			},
			{ immediate: true }
		);
	});

	onScopeDispose(() => {
		scope.stop();
	});
}

type ThemeVars = Exclude<GlobalThemeOverrides["common"], undefined>;
type ThemeVarsKeys = keyof ThemeVars;

/** 添加css vars至html */
function addThemeCssVarsToHtml(themeVars: ThemeVars) {
	const keys = Object.keys(themeVars) as ThemeVarsKeys[];
	const style: string[] = [];
	keys.forEach((key) => {
		const color = themeVars[key];

		if (color) {
			const { r, g, b } = getRgbOfColor(color);
			style.push(`--${kebabCase(key)}: ${r},${g},${b}`);

			if (key === "primaryColor") {
				const colorPalettes = getColorPalettes(color);

				colorPalettes.forEach((palette, index) => {
					const { r: pR, g: pG, b: pB } = getRgbOfColor(palette);
					style.push(`--${kebabCase(key)}${index + 1}: ${pR},${pG},${pB}`);
				});
			}
		}
	});
	const styleStr = style.join(";");
	document.documentElement.style.cssText += styleStr;
}
