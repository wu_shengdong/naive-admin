import { getNaiveThemeOverrides, initThemeSettings } from "./helpers";

import { darkTheme } from "naive-ui";

type ThemeState = Theme.Setting;

export const useThemeStore = defineStore("useThemeStore", {
	state: (): ThemeState => initThemeSettings(),
	getters: {
		/** naiveUI的主题配置 */
		naiveThemeOverrides(state) {
			const overrides = getNaiveThemeOverrides({
				primary: state.themeColor,
				...state.otherColor,
			});
			return overrides;
		},
		/** naive-ui暗黑主题 */
		naiveTheme(state) {
			return state.darkMode ? darkTheme : undefined;
		},
	},
	actions: {
		/** 设置scrollbar的实例 */
		setScrollbarInstance(scrollbar: ScrollbarInst) {
			this.scrollbar = scrollbar;
		},
		/** 设置侧边栏折叠状态 */
		setSiderCollapse(collapse: boolean) {
			this.sider.collapsed = collapse;
		},
		/** 折叠/展开 侧边栏折叠状态 */
		toggleSiderCollapse() {
			this.sider.collapsed = !this.sider.collapsed;
		},
		/** 设置暗黑模式 */
		setDarkMode(darkMode: boolean) {
			this.darkMode = darkMode;
		},
		/** 设置自动跟随系统主题 */
		setFollowSystemTheme(visible: boolean) {
			this.followSystemTheme = visible;
		},
		/** 设置自动跟随系统主题 */
		setIsCustomizeDarkModeTransition(isCustomize: boolean) {
			this.isCustomizeDarkModeTransition = isCustomize;
		},
		/** 自动跟随系统主题 */
		setAutoFollowSystemMode(darkMode: boolean) {
			if (this.followSystemTheme) {
				this.darkMode = darkMode;
			}
		},
	},
});
