import type { ScrollbarInst } from "naive-ui";

/** app状态 */
interface AppState {
	/** 布局状态 */
	scrollbar: ScrollbarInst | null;
	/** 重新加载标记 */
	reloadFlag: boolean;
}

export const useAppStore = defineStore("useAppStore", {
	state: (): AppState => ({
		scrollbar: null,
		reloadFlag: true,
	}),
	actions: {
		/** 设置scrollbar的实例 */
		setScrollbarInstance(scrollbar: ScrollbarInst) {
			this.scrollbar = scrollbar;
		},
		/** 重置滚动条行为 */
		resetScrollBehavior() {
			setTimeout(() => {
				this.scrollbar?.scrollTo({ left: 0, top: 0 });
			}, 250);
		},
		/** 重新加载页面 */
		handleReload() {
			this.reloadFlag = false;
			nextTick(() => {
				this.reloadFlag = true;
				this.resetScrollBehavior();
			});
		},
	},
});
