import logoImage from '@/assets/logo.png';
import loginImage from '@/assets/account-logo.png';

export const websiteConfig = Object.freeze({
  title: 'NaiveUiAdmin',
  logo: logoImage,
  loginImage: loginImage,
  loginDesc: 'Naive Ui Admin中后台前端/设计解决方案',
});
