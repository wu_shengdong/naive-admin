import type { RouteComponent } from "vue-router";

export const views: Record<
	PageRoute.LastDegreeRouteKey,
	RouteComponent | (() => Promise<{ default: RouteComponent }>)
> = {
	dashboard_analysis: () => import("./dashboard/analysis/index.vue"),
	404: import("./404/index.vue"),
	"not-found": () => import("./not-found/index.vue"),
};
