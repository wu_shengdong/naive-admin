import { setupRouter } from "@/router";
import "uno.css";
import { createApp } from "vue";
import App from "./App.vue";
import AppLoading from "./components/common/app-loading.vue";
import { setupI18n } from "./locales";
import { setupStore } from "./store";
import "./styles/css/global.css";

async function setupApp() {
	// app loading
	const appLoading = createApp(AppLoading);

	appLoading.mount("#appLoading");

	const app = createApp(App);

	// 挂载状态管理
	setupStore(app);

	// 路由准备就绪后挂载 APP 实例
	// https://router.vuejs.org/api/interfaces/router.html#isready
	await setupRouter(app);

	setupI18n(app);

	app.mount("#app");
}

setupApp();
