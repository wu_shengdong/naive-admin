import unocss from "@unocss/vite";
import vue from "@vitejs/plugin-vue";
import type { PluginOption } from "vite";
import VueDevtools from "vite-plugin-vue-devtools";
import mock from "./mock";
import unplugin from "./unplugin";
/**
 * vite插件
 * @param viteEnv - 环境变量配置
 */
export function setupVitePlugins(
	viteEnv: ImportMetaEnv
): (PluginOption | PluginOption[])[] {
	const plugins = [
		vue({
			script: {
				defineModel: true,
			},
		}),
		VueDevtools(),
		...unplugin(viteEnv),
		unocss(),
		mock(viteEnv),
	];
	return plugins;
}
